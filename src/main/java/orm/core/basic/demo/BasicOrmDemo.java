package orm.core.basic.demo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import orm.core.basic.demo.entity.Comment;
import orm.core.basic.demo.entity.Community;
import orm.core.basic.demo.entity.Donation;
import orm.core.basic.demo.entity.Project;
import orm.core.basic.demo.entity.ServiceEvent;
import orm.core.basic.demo.entity.Skill;
import orm.core.basic.demo.entity.Tool;
import orm.core.basic.demo.entity.User;

public class BasicOrmDemo {

	public static void main(String[] args) {
		try {
			Tool tool = new Tool();
			// Note: id generated by Hibernate
			tool.setName( "Hammer" );
			insertTool( tool );
			List<Tool> tools = new ArrayList<>();
			tools.add( tool );
			
			Skill skill = new Skill();
			// Note: id generated by Hibernate
			skill.setName( "Hammering Things" );
			insertSkill( skill );
			List<Skill> skills = new ArrayList<Skill>();
			skills.add( skill );
			
			User user = new User();
			// Note: id generated by Hibernate
			user.setName( "Brett Meyer" );
			user.setEmail( "foo@foo.com" );
			user.setPhone( "123-456-7890" );
			user.setTools( tools );
			user.setSkills( skills );
			
			insertUser( user );
			
			user = getUser(3);
			System.out.println( user.toString() );
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.exit(0);
	}

	private static void insertTool(Tool tool) throws SQLException {
		Session session = openSession();
		session.getTransaction().begin();
		session.persist( tool );
		session.getTransaction().commit();
	}
	
	private static void insertSkill(Skill skill) throws SQLException {
		Session session = openSession();
		session.getTransaction().begin();
		session.persist( skill );
		session.getTransaction().commit();
	}
	
	private static void insertUser(User user) throws Exception {
		Session session = openSession();
		session.getTransaction().begin();
		session.persist( user ); // cascades the tool & skill relationships
		session.getTransaction().commit();
	}
	
	private static User getUser(int id) throws SQLException {
		Session session = openSession();
		
		User user = (User) session.get( User.class, id );
		
//		Query query = session.createQuery( "SELECT u FROM User u WHERE u.id=:id" );
//		query.setParameter( "id", id );
//		User user = (User) query.uniqueResult();
		
//		User user = (User) session.createCriteria( User.class )
//				.add( Restrictions.eq( "id", id ) )
//				.uniqueResult();
		
		Hibernate.initialize( user.getTools() );
		Hibernate.initialize( user.getSkills() );
		
		session.close();
		
		return user;
	}
	
    private static SessionFactory sessionFactory = null;
	
	private static Session openSession() {
		if (sessionFactory == null) {
			final Configuration configuration = new Configuration();
			configuration.addAnnotatedClass( User.class );
			configuration.addAnnotatedClass( Tool.class );
			configuration.addAnnotatedClass( Skill.class );
			configuration.addAnnotatedClass( Community.class );
			configuration.addAnnotatedClass( Donation.class );
			configuration.addAnnotatedClass( Comment.class );
			configuration.addAnnotatedClass( ServiceEvent.class );
			configuration.addAnnotatedClass( Project.class );
			
			sessionFactory = configuration.buildSessionFactory( new StandardServiceRegistryBuilder().build() );
		}
		return sessionFactory.openSession();
	}
}
